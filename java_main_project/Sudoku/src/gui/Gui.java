


package gui;


import java.util.ArrayList;
import java.util.HashMap;


import javax.swing.*;
import java.awt.*;


import sudoku.SudokuSolver;
import sudoku.Sudoku;



public class Gui {
	private ArrayList<JTextField> cells = null;
	
	private JFrame window = null;
	
	private JButton clearButton = null;
	private JButton solveButton = null;
	
	
	private static Color[] colors = {
		getColor(177, 16, 250),
		getColor(10,  16, 250)
	};
	
	
	private static Font cellFont   = new Font(Font.MONOSPACED, Font.ITALIC, 24);
	private static Font buttonFont = cellFont;
	
	public Gui() {
		window = new JFrame("Sudoku Solver by Harald Förare");
		window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);		
		
		
		// Flat storage of cells
		cells = new ArrayList<JTextField>();
				
		
		
		// Sudoku input
		{
			// Cell container
			JPanel row = null;
			
			
			// Create all cells
			for (int cellIndex = 0; cellIndex < 9 * 9; cellIndex += 1) {
				
				// Create grid for every row of inputs, this is to make buttons at bottom be same height as one row
				if (cellIndex % 9 == 0) {
					row = new JPanel();
					row.setLayout(new GridLayout(1, 9));
					window.add(row);
				}
				
				
				// Core component, all state is stored here
				JTextField cell = new JTextField();
				
				
				// Save references
				row.add(cell);
				cells.add(cell);
				
				
				// Set font
				cell.setFont(cellFont);
				
				
				// Set center text alignment
				cell.setHorizontalAlignment(JTextField.CENTER);
								
				
				// Set background color
				{
					int coloringRow   = (cellIndex / 9) / 3;
					int coloringCol   = (cellIndex % 9) / 3;
					int coloringIndex = (coloringRow * 3 + coloringCol) % 2;
									
					Color color = colors[coloringIndex];
					
					cell.setBackground(color);
					
					// Effectively disable caret and selection by setting their colors to same as background
					cell.setCaretColor(color);
					cell.setSelectionColor(color);
				}
				
				
				// Restrict input
				cell.getDocument().addDocumentListener(
					new CellInputRestrictor(cell)
				);
				
				
				// Enable arrow key navigation
				cell.addKeyListener(
					new ArrowNavigationController(cellIndex, cells)
				);
			}
		}
		
		
		
		// Bottom controls
		{
			JPanel controlPanel = new JPanel();
			controlPanel.setLayout(new GridLayout(1, 2));
			window.add(controlPanel);
			
			{
				clearButton = new JButton("clear");
				controlPanel.add(clearButton);
				clearButton.setFont(buttonFont);
				clearButton.addActionListener(e -> {
					onClearButtonPress();
				});
			}
			
			{
				solveButton = new JButton("solve");
				controlPanel.add(solveButton);
				solveButton.setFont(buttonFont);
				solveButton.addActionListener(e -> {
					onSolveButtonPress();
				});
			}
		}

		
		
		// Show GUI (stateful)
		window.setLayout(new GridLayout(10, 1));
		window.setSize(520, 640);
		window.setVisible(true);
	}
	
	
	public Gui(String source) {
		
		// Call default constructor
		this();
		
		
		// Load Sudoku from source code string
		{
			// Map of source code characters to data
			HashMap<String, String> translationMap = new HashMap<String, String>();
			for (int n = 1; n <= 9; n += 1) {
				Integer asObject = n;
				String  asString = asObject.toString();
				
				translationMap.put(asString, asString);
			}
			
			translationMap.put("-", "");
			
			
			// Read source code
			int cellIndex = 0;
			for (int n = 0; n < source.length(); n += 1) {
				String srcChar = source.substring(n, n + 1);

				if (!translationMap.containsKey(srcChar)) {continue;}
				
				String dstChar = translationMap.get(srcChar);
				cells.get(cellIndex).setText(dstChar);
				cellIndex += 1;
			}
		}
	}
	
	

	private void onClearButtonPress() {		
		for (JTextField cell : cells) {
			cell.setText("");
		}
	}
	
	
	
	private void onSolveButtonPress() {	
		
		// Disable inputs and indicate work is being done
		setEnabled(false);
    	window.setCursor(new Cursor(Cursor.WAIT_CURSOR));


		
		// Solve on GUI thread
		// No inputs should be made while working
		// Done to visually disable GUI before starting work
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {

				// Create Sudoku
				SudokuSolver sudoku = new Sudoku();
				
				
				// Fill with data
				{
					HashMap<String, Integer> translationMap = new HashMap<String, Integer>();
					{
						for (int n = 1; n <= 9; n += 1) {
							Integer asObject = n;
							String  asString = asObject.toString();
							translationMap.put(asString, asObject);
						}
						
						translationMap.put("", 0);
					}
					
					
					for (int r = 0; r < 9; r += 1) {
						for (int c = 0; c < 9; c += 1) {				
							String      guiData = cells.get(r * 9 + c).getText();
							Integer backendData = translationMap.get(guiData);
							sudoku.add(r, c, backendData);
						}
					}
				}
				
				
				// Solve Sudoku
				boolean solutionWasFound = sudoku.solve();
								
				
				// Present to user
				if (solutionWasFound) {
					HashMap<Integer, String> translationMap = new HashMap<Integer, String>();
					
					for (int n = 1; n <= 9; n += 1) {
						Integer asObject = n;
						String  asString = asObject.toString();
						
						translationMap.put(asObject, asString);
					}
					
					translationMap.put(0, "");
					
					
					for (int n = 0; n < 9 * 9; n += 1) {
						int r = n / 9;
						int c = n % 9;
						
						Integer rawValue = sudoku.get(r, c);
						String translatedValue = translationMap.get(rawValue);
						
						cells.get(n).setText(translatedValue);
					}
				}
				
				else {
					JOptionPane optionPane = new JOptionPane("No solution found");
					JDialog dialog = optionPane.createDialog(null, "Sudoku Solver");
					dialog.setVisible(true);
				}
				
				
				// Schedule GUI re-enable and default cursor
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						setEnabled(true);
				    	window.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					}
				});
			}
		});
		
		
	}
	
	
	
	private void setEnabled(boolean isEnabled) {
		clearButton.setEnabled(isEnabled);
		solveButton.setEnabled(isEnabled);
		
		for (JTextField cell : cells) {
			cell.setEnabled(isEnabled);
		}
	}
	
	


	private static Color getColor(int h, int s, int b) {
		return Color.getHSBColor(
			(float)h / 255.0f,
			(float)s / 255.0f,
			(float)b / 255.0f
		);
	}
}




