

package gui;


import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import java.util.HashSet;



public class CellInputRestrictor implements DocumentListener {
	private static HashSet<String> allowedChars = null;

	private boolean    doCaptureEvents = true;
	private JTextField cell            = null;
	private String     value           =   "";
	
	
	public CellInputRestrictor(JTextField cell) {
		this.cell = cell;
		
		
		// Build allowed chars map for input verification
		if (allowedChars == null) {
			allowedChars = new HashSet<String>();
			
			
			// Allow numbers 1 through 9
			for (int n = 1; n <= 9; n += 1) {
				Integer number = n;
				allowedChars.add(number.toString());
			}
			
			
			// Always allow empty string
			allowedChars.add("");
		}
		
		
		// Prime state
		onInput();
	}

	
	@Override
	public void insertUpdate(DocumentEvent e) {		
		onInput();
	}

	@Override
	public void removeUpdate(DocumentEvent e) {		
		onInput();
	}

	@Override
	public void changedUpdate(DocumentEvent e) {		
		onInput();
	}

	
	private void onInput() {
		if (!doCaptureEvents) {
			return;
		}
		
		
		String rawText = cell.getText();
		
		
		// Trim value
		String newValue = "";
		if (rawText.length() > 0) {  // Get last character of text
			newValue = rawText.substring(rawText.length() - 1);
		}
		

		// Detect change
		boolean isNewValue = newValue != value;
		
		
		// Restrict input
		if (!allowedChars.contains(newValue)) {
			newValue = "";
		}
		
		
		// Set new state
		value = newValue;
		
		
		// Update UI on event loop thread
		if (isNewValue) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {

					
					doCaptureEvents = false;
					cell.setText(value);
					
					
					// Schedule event capture reset
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							doCaptureEvents = true;
						}
					});
				}
			});
		}
	}
}




