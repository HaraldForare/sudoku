package sudoku;

import java.util.Iterator;

public class Sudoku implements SudokuSolver {

	@Override
	public boolean solve() {
		stepCounter = 0;
		
		int[][] oldState = getMatrix();
		
		// Validate board (do this here instead of in recursion for immediate feedback)
		boolean solutionWasFound = isValid();
		
		
		// Recursively solve
		if (solutionWasFound) {
			solutionWasFound = solve(0, 0);
		}
		
		
		// Logging
		System.out.print("Solution was ");
		if (!solutionWasFound) {System.out.print("not ");}
		System.out.print("found in ");
		System.out.print(stepCounter);
		System.out.print(" steps\n");
		
		
		// Reset state
		if (!solutionWasFound) {
			setMatrix(oldState);
		}
		
		return solutionWasFound;
	}

	
	@Override
	public void add(int row, int col, int digit) {
		
		// Input validation
		{
			if ((digit < 0) || (digit > 9)) {
				throw new IllegalArgumentException("Digit out of range");
			}
			
			if ((row < 0) || (row > 8)) {
				throw new IllegalArgumentException("Row out of range");
			}
			
			if ((col < 0) || (col > 8)) {
				throw new IllegalArgumentException("Column out of range");
			}
		}
		
		
		matrix[row][col] = digit;
	}

	
	@Override
	public void remove(int row, int col) {
		
		// Input validation
		{
			if ((row < 0) || (row > 8)) {
				throw new IllegalArgumentException("Row out of range");
			}
			
			if ((col < 0) || (col > 8)) {
				throw new IllegalArgumentException("Column out of range");
			}
		}
		
		
		matrix[row][col] = 0;
	}

	
	@Override
	public int get(int row, int col) {
		
		// Input validation
		{
			if ((row < 0) || (row > 8)) {
				throw new IllegalArgumentException("Row out of range");
			}
			
			if ((col < 0) || (col > 8)) {
				throw new IllegalArgumentException("Column out of range");
			}
		}
		
		
		return matrix[row][col];
	}

	
	@Override
	public boolean isValid() {
		for (int r = 0; r < 9; r += 1) {
			for (int c = 0; c < 9; c += 1) {
				if (!isValid(r, c)) {
					return false;
				}
			}
		}
		
		return true;
	}

	
	@Override
	public void clear() {
		for (int r = 0; r < 9; r += 1) {
			for (int c = 0; c < 9; c += 1) {
				matrix[r][c] = 0;
			}
		}
	}

	
	@Override
	public void setMatrix(int[][] m) {
		
		// Input validation
		{
			if (m.length != 9) {
				throw new IllegalArgumentException("Number of rows is not 9");
			}
			
			for (int r = 0; r < 9; r += 1) {
				if (m[r].length != 9) {
					throw new IllegalArgumentException("Number of columns is not 9");
				}
				
				for (int c = 0; c < 9; c += 1) {
					int value = m[r][c];

					if ((value < 0) || (value > 9)) {
						throw new IllegalArgumentException("Value out of range");
					}
				}
			}
		}
		
		
		for (int r = 0; r < 9; r += 1) {
			for (int c = 0; c < 9; c += 1) {
				matrix[r][c] = m[r][c];
			}
		}
	}
	

	@Override
	public int[][] getMatrix() {
		int[][] matrixCopy = new int[9][9];
		
		for (int r = 0; r < 9; r += 1) {
			for (int c = 0; c < 9; c += 1) {
				matrixCopy[r][c] = matrix[r][c];
			}
		}
		
		return matrixCopy;
	}

	

	
	
	public Sudoku() {
		clear();
	}
	
	
	
	
	// Private stuff
	
	
	private int[][] matrix = new int[9][9];
	

	private int stepCounter = 0;
	
	
	// Using row and column is retarded
	private boolean solve(int r, int c) {
		return solve(r * 9 + c);
	}
	
	
	
	private boolean isValid(int r, int c) {
		int value = matrix[r][c];
		
		
		// Not filled in is always valid
		if (value == 0) {
			return true;
		}
		
		
		// Block offsets
		int blockRowOffset = (r / 3) * 3;
		int blockColOffset = (c / 3) * 3;
		
		
		// Check all regions for duplicates
		for (int n = 0; n < 9; n += 1) {
			
			// Horizontal
			if (n != c) {
				if (matrix[r][n] == value) {
					return false;
				}
			}
			
			// Vertical
			if (n != r) {
				if (matrix[n][c] == value) {
					return false;
				}
			}
			
			// Block
			{
				int blockRowIndex = n / 3 + blockRowOffset;
				int blockColIndex = n % 3 + blockColOffset;
				
				// Don't check self
				if (blockRowIndex == r) {continue;}
				if (blockColIndex == c) {continue;}
				
				if (matrix[blockRowIndex][blockColIndex] == value) {
					return false;
				}
			}
		}
		
		// No duplicates were found
		return true;
	}
	
	
	private boolean solve(int n) {
		
		
		
		// Progress messages
		stepCounter += 1;

		if (stepCounter % 10000 == 0) {
			float percentage = (float)n / (float)(9 * 9);
			
			System.out.print(stepCounter);
			System.out.print("\tsteps,\t");
			System.out.print(100.0f * percentage);
			System.out.print(" %\n");
		}
		
		// Solution was found
		if (n >= 9 * 9) {
			return true;
		}
		
		
		// Unpack index into row and columns, much smarter to do it this way around
		int r = n / 9;
		int c = n % 9;
		
		
		// Skip if location is already filled in
		if (matrix[r][c] != 0) {			
			return solve(n + 1);
		}
		
		
		// Test values
		for (int proposal = 1; proposal <= 9; proposal += 1) {
			
			// Set value in matrix so it is visible to deeper recursion
			int oldValue = matrix[r][c];
			matrix[r][c] = proposal;
			
			
			// Continue to next proposal if this isn't valid
			if (!isValid(r, c)) {
				matrix[r][c] = oldValue;
				continue;
			}
			
			
			// Recursively continue
			boolean solutionWasFound = solve(n + 1);
			
			
			// Collapse stack
			if (solutionWasFound) {
				return true;
			}
			
			
			// Solution was not found, reset location in matrix
			matrix[r][c] = oldValue;
		}
		
		
		// No solution was found
		return false;
	}
}




