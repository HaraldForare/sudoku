package src;

import gui.Gui;


public class Main {
	public static void main(String[] args) {
		
		Gui gui = new Gui(String.join("\n", new String[] {
	            "-5-  9-4  ---",
	            "---  ---  813",
	            "2--  ---  ---",
	            
	            "6-2  --8  -5-",
	            "--5  ---  -36",
	            "---  --1  9--",
	            
	            "--8  -4-  3--",
	            "-4-  ---  1--",
	            "---  1-6  ---"
		}));
	}
}
