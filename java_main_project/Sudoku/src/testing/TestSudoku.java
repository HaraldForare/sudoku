

package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import sudoku.SudokuSolver;
import sudoku.Sudoku;



class TestSudoku {
	
	private SudokuSolver sudoku = null;

	
	
	@BeforeEach
	void setUp() throws Exception {
		sudoku = new Sudoku();
	}

	
	@Test
	void testSolveEmpty() {
		assertTrue(sudoku.solve());
	}

	
	void assertIsClear() {
		int[][] matrix = sudoku.getMatrix();

		
		for (int n = 0; n < 9 * 9; n += 1) {
			int r = n / 9;
			int c = n % 9;
			
			assertTrue(matrix[r][c] == 0);
		}
	}
	
	
	void assertCellContains(int row, int col, int value) {
		assertTrue(sudoku.getMatrix()[row][col] == value);
	}
	
	
	@Test
	void testEmptyInitialization() {
		assertIsClear();
	}
	
	
	@Test
	void testClear() {
		sudoku.add(1, 3, 9);
		sudoku.add(7, 1, 1);
		sudoku.add(7, 8, 8);
		
		sudoku.clear();
		
		assertIsClear();
	}
	
	
	@Test
	void testAdd() {
		sudoku.add(5, 7, 3);
		assertCellContains(5, 7, 3);
		
		sudoku.add(5, 7, 0);
		assertCellContains(5, 7, 0);
		
		assertCellContains(5, 8, 0);
		
		sudoku.add(5, 8, 1);
		assertCellContains(5, 8, 1);
	}
	
	
	@Test
	void testRemove() {
		sudoku.add(3, 6, 6);
		sudoku.remove(3, 6);
		assertCellContains(3, 6, 0);
	}
	
	
	@Test
	void testGet() {
		assertTrue(sudoku.get(8, 7) == 0);
		
		sudoku.add(2, 2, 8);
		assertTrue(sudoku.get(2, 2) == 8);
		assertTrue(sudoku.get(4, 5) == 0);
	}
	
	
	@Test
	void testSolveFig1() {
		int[][] question = {
			{0, 0, 8,	0, 0, 9,	0, 6, 2},
			{0, 0, 0,	0, 0, 0,	0, 0, 5},
			{1, 0, 2,	5, 0, 0,	0, 0, 0},
			
			{0, 0, 0,	2, 1, 0,	0, 9, 0},
			{0, 5, 0,	0, 0, 0,	6, 0, 0},
			{6, 0, 0,	0, 0, 0,	0, 2, 8},
			
			{4, 1, 0,	6, 0, 8,	0, 0, 0},
			{8, 6, 0,	0, 3, 0,	1, 0, 0},
			{0, 0, 0,	0, 0, 0,	4, 0, 0}
		};
		
		int[][] correctAnswer = {
			{5, 4, 8,	1, 7, 9,	3, 6, 2},
			{3, 7, 6,	8, 2, 4,	9, 1, 5},
			{1, 9, 2,	5, 6, 3,	8, 7, 4},
			
			{7, 8, 4,	2, 1, 6,	5, 9, 3},
			{2, 5, 9,	3, 8, 7,	6, 4, 1},
			{6, 3, 1,	9, 4, 5,	7, 2, 8},
			
			{4, 1, 5,	6, 9, 8,	2, 3, 7},
			{8, 6, 7,	4, 3, 2,	1, 5, 9},
			{9, 2, 3,	7, 5, 1,	4, 8, 6}
		};
		
		
		sudoku.setMatrix(question);
		assertTrue(sudoku.solve());
		int[][] producedAnswer = sudoku.getMatrix();
		assertArrayEquals(producedAnswer, correctAnswer);
	}
	
	
	@Test
	void testIsValidEmpty() {
		assertTrue(sudoku.isValid());
	}
	
	
	@Test
	void testIsValidFullySolvedFig1() {
		testSolveFig1();
		
		assertTrue(sudoku.isValid());
	}
	
	
	@Test
	void testSolveWithBlockDuplicates() {
		sudoku.add(4, 4, 3);
		sudoku.add(5, 5, 3);
		
		assertFalse(sudoku.solve());
	}
	
	
	@Test
	void testSolveWithRowDuplicates() {
		sudoku.add(8, 1, 8);
		sudoku.add(8, 8, 8);
		
		assertFalse(sudoku.solve());
	}
	
	@Test
	void testSolveWithColumnDuplicates() {
		sudoku.add(5, 4, 9);
		sudoku.add(2, 4, 9);
		
		assertFalse(sudoku.solve());
	}
	
	@Test
	void testIsValidWithBlockDuplicates() {
		sudoku.add(4, 4, 3);
		sudoku.add(5, 5, 3);
		
		assertFalse(sudoku.isValid());
	}
	
	
	@Test
	void testIsValidWithRowDuplicates() {
		sudoku.add(8, 1, 8);
		sudoku.add(8, 8, 8);
		
		assertFalse(sudoku.isValid());
	}
	
	@Test
	void testIsValidWithColumnDuplicates() {
		sudoku.add(5, 4, 9);
		sudoku.add(2, 4, 9);
		
		assertFalse(sudoku.isValid());
	}
}








