

package sudoku;



public interface SudokuSolver {
	
	/**
	 * Solves Sudoku. Internal state is filled with solution
	 * 
	 * @returns True if solution was found
	 */
	boolean solve();

	
	/**
	 * Puts digit in the box row, col.
	 * 
	 * @param row   The row
	 * @param col   The column
	 * @param digit The digit to insert in box row, col
	 * @throws IllegalArgumentException if row, col or digit is outside the range
	 *                                  [0..9]
	 */
	void add(int row, int col, int digit);

	
	/**
	 * Removes digit from the Sudoku
	 * 
	 * @param row   The row
	 * @param col   The column
	 */
	void remove(int row, int col);

	
	/**
	 * Gets digit at specified cell in Sudoku.
	 * 
	 * @param row   The row
	 * @param col   The column
	 */
	int get(int row, int col);

	
	/**
	 * Checks that all filled in digits follow the the Sudoku rules.
	 * 
	 * @returns True if Sudoku is valid
	 */
	boolean isValid();

	
	/**
	 * Clears all digits in Sudoku. Equivalent to calling clear() for all cells
	 * 
	 * 
	 */
	void clear();

	
	/**
	 * Fills the grid with the digits in m. The digit 0 represents an empty box.
	 * 
	 * @param m the matrix with the digits to insert
	 * @throws IllegalArgumentException if m has the wrong dimension or contains
	 *                                  values outside the range [0..9]
	 */
	void setMatrix(int[][] m);

	
	/**
	 * Gets a copy of the internal memory buffer of the Sudoku.
	 * 
	 * SudokuSolver.setMatrix(SudokuSolver.getMatrix()) never modifies SudokuSolver.
	 * 
	 * 
	 */
	int[][] getMatrix();
}



