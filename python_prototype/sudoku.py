

import numpy as np


class Sudoku:
    def __init__(self, startString = None):
        self.board = np.zeros((9, 9), np.int8)

        if startString is not None:
            startString = startString.replace(" ",  "")
            startString = startString.replace("\n", "")

            for index, char in enumerate(startString):
                if not char.isnumeric():
                    continue

                rowIndex, colIndex = Sudoku._getRowAndCol(index)

                value = int(char)
                assert Sudoku._isAllowed(value)
                self.board[rowIndex][colIndex] = value





    @staticmethod
    def _getRowAndCol(index):
        return index // 9, index % 9



    @staticmethod
    def _isAllowed(value):
        return 1 <= value <= 9



    class NoSolution(Exception):
        pass



    def _getBlock(self, row, col):
        row = (row // 3) * 3
        col = (col // 3) * 3
        block = self.board[row: row + 3, col: col + 3]
        return block



    def _getRegions(self, row, col):
        block      = self._getBlock(row, col)
        horizontal = self.board[row]
        vertical   = self.board[:, col]

        outputs = (block, horizontal, vertical)

        # Check for disqualifications
        if False:
            for output in outputs:
                if len(set(output.flat)) < len(output.flat):
                    raise Sudoku.NoSolution


        return outputs



    def solve(self):
        class SolutionFound(Exception):
            pass


        def recursiveSolve(index):

            # Solution found
            if index >= 9 * 9:
                raise SolutionFound


            # Unpack for easier programming
            row, col = Sudoku._getRowAndCol(index)


            # Continue to next if location is already filled in
            if Sudoku._isAllowed(self.board[row][col]):
                recursiveSolve(index + 1)
                return


            # Get lookup regions of board
            block, horizontal, vertical = self._getRegions(row, col)


            # Test numbers to fill in
            for value in range(1, 9 + 1):

                # Check for disqualifications
                if value in block:
                    continue

                if value in horizontal:
                    continue

                if value in vertical:
                    continue


                # Set value in board for deeper recursion to see
                oldValue = self.board[row][col]
                self.board[row][col] = value


                # Continue recursively
                recursiveSolve(index + 1)


                # Unset value
                self.board[row][col] = oldValue


        try:
            recursiveSolve(0)
            raise Sudoku.NoSolution


        except SolutionFound:
            return self




    def __str__(self):
        rows = list()

        for rowIndex in range(9):
            row = list()
            for colIndex in range(9):
                cell = self.board[rowIndex][colIndex]
                if Sudoku._isAllowed(cell):
                    row.append(str(cell))

                else:
                    row.append("-")

                if (colIndex + 1) % 3 == 0:
                    if colIndex != 8:
                        row.append("  ")

            row = " ".join(row)
            rows.append(row)

            if (rowIndex + 1) % 3 == 0:
                if rowIndex != 8:
                    rows.append("")


        asString = "\n".join(rows)
        return asString




if __name__ == "__main__":
    import time

    sudokus = [
        """
            ---  ---  ---
            ---  ---  ---
            ---  ---  ---
            
            ---  ---  ---
            ---  ---  ---
            ---  ---  ---
            
            ---  ---  ---
            ---  ---  ---
            ---  ---  ---
        """,

        """
            ---  ---  ---
            -1-  ---  ---
            ---  ---  ---

            ---  ---  ---
            ---  ---  -8-
            ---  ---  ---

            ---  ---  ---
            ---  ---  ---
            ---  ---  ---
        """,

        # Hard difficulty
        """
            -5-  9-4  ---
            ---  ---  813
            2--  ---  ---
            
            6-2  --8  -5-
            --5  ---  -36
            ---  --1  9--
            
            --8  -4-  3--
            -4-  ---  1--
            ---  1-6  ---
        """,

        # Medium difficulty
        """
            4--  -51  8--
            -8-  6--  -4-
            ---  48-  196
            
            ---  51-  37-
            -4-  -2-  ---
            -5-  3-7  42-
            
            -13  -7-  98-
            69-  1-8  25-
            --2  ---  613
        """,

        # No solution
        """
            ---  ---  ---
            -1-  ---  ---
            --1  ---  ---
            
            ---  ---  ---
            ---  ---  ---
            ---  ---  ---
            
            ---  ---  ---
            ---  ---  ---
            ---  ---  ---
        """,

        # Pathological
        """
            ---  ---  ---
            ---  --3  -85
            --1  -2-  ---
            
            ---  5-7  ---
            --4  ---  1--
            -9-  ---  ---
            
            5--  ---  -73
            --2  -1-  ---
            ---  -4-  --9
        """
    ]


    sudokus = [Sudoku(source) for source in sudokus]


    for sudoku in sudokus:
        print("Solving Sudoku:")
        print(sudoku)
        print("...")
        print("")

        startTime = time.time()

        try:
            sudoku.solve()
            print("Solution found:")
            print(sudoku)

        except Sudoku.NoSolution:
            print("No solution found!")

        stopTime = time.time()
        elapsedTime = stopTime - startTime

        print(f"{round(elapsedTime, 2)} seconds")
        print("")
        print("")
        print("")

