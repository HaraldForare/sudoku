

package gui;


import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.SwingUtilities;


import java.util.ArrayList;
import javax.swing.JTextField;


public class ArrowNavigationController implements KeyListener {

	private ArrayList<JTextField> cells;
	private int                     row;
	private int                     col;
	
	
	public ArrowNavigationController(int index, ArrayList<JTextField> cells) {
		this.cells = cells;
		
		row = index / 9;
		col = index % 9;
	}
	

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		
		
		int newRow = row;
		int newCol = col;
		
	    if      (keyCode == KeyEvent.VK_RIGHT) {newCol += 1;}
	    else if (keyCode == KeyEvent.VK_LEFT)  {newCol -= 1;}
	    else if (keyCode == KeyEvent.VK_UP)    {newRow -= 1;}
	    else if (keyCode == KeyEvent.VK_DOWN)  {newRow += 1;}

	    
	    // Don't do anything for unbound keys
	    else {return;}
	    
	    
	    // Handle wrapping
	    while (newRow <  0) {newRow += 9;}
	    while (newRow >= 9) {newRow -= 9;}
	    while (newCol <  0) {newCol += 9;}
	    while (newCol >= 9) {newCol -= 9;}
	    
	    
	    // Get new focused cell
	    int newIndex = newRow * 9 + newCol;
	    JTextField newCell = cells.get(newIndex);
	    
	    
	    // Schedule focus change
	    SwingUtilities.invokeLater(new Runnable() {
	    	public void run() {
	    		newCell.requestFocusInWindow();
	    	}
	    });
	}
	
	
	// Do nothing for these, needed only to satisfy interface
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void keyReleased(KeyEvent e) {}
}



